{
  "swagger" : "2.0",
  "info" : {
    "description" : "Channel manipulation API",
    "version" : "1.0.0",
    "title" : "Baby IPSM API"
  },
  "host" : "localhost:8080",
  "basePath" : "/",
  "schemes" : [ "http" ],
  "consumes" : [ "application/json" ],
  "produces" : [ "application/json" ],
  "paths" : {
    "/alignments" : {
      "get" : {
        "tags" : [ "Alignments" ],
        "summary" : "List alignments",
        "description" : "Lists alignment pairs supported by IPSM.",
        "parameters" : [ ],
        "responses" : {
          "200" : {
            "description" : "supported alignment pairs",
            "schema" : {
              "type" : "array",
              "items" : {
                "$ref" : "#/definitions/Alignment"
              }
            }
          }
        }
      }
    },
    "/channels" : {
      "get" : {
        "tags" : [ "Channels" ],
        "summary" : "List active IPSM channels",
        "description" : "Tha endpoint returns the list of all IPSM channels which are currently\nactive.\nThe Products endpoint returns information about the *Uber* products\noffered at a given location. The response includes the display name\nand other details about each product, and lists the products in the\nproper display order.\n",
        "parameters" : [ ],
        "responses" : {
          "200" : {
            "description" : "array of channel information",
            "schema" : {
              "type" : "array",
              "items" : {
                "$ref" : "#/definitions/ChannelInfo"
              }
            }
          }
        }
      },
      "post" : {
        "tags" : [ "Channels" ],
        "summary" : "Create new channel",
        "consumes" : [ "application/json" ],
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "in" : "body",
          "name" : "conf",
          "description" : "channel configuration",
          "required" : true,
          "schema" : {
            "$ref" : "#/definitions/ChannelConfig"
          }
        } ],
        "responses" : {
          "201" : {
            "description" : "Channel created successfully",
            "schema" : {
              "$ref" : "#/definitions/Response"
            }
          },
          "400" : {
            "description" : "channel exists or wrong arguments",
            "schema" : {
              "$ref" : "#/definitions/Error"
            }
          }
        }
      }
    },
    "/channels/{channelId}" : {
      "delete" : {
        "tags" : [ "Channels" ],
        "summary" : "Delete channel based on the ID",
        "parameters" : [ {
          "name" : "channelId",
          "in" : "path",
          "description" : "ID of channel to delete",
          "required" : true,
          "type" : "string"
        } ],
        "responses" : {
          "204" : {
            "description" : "channel deleted"
          },
          "400" : {
            "description" : "channel not found",
            "schema" : {
              "$ref" : "#/definitions/Error"
            }
          },
          "500" : {
            "description" : "closing channel failed",
            "schema" : {
              "$ref" : "#/definitions/Error"
            }
          }
        }
      }
    }
  },
  "definitions" : {
    "ChannelInfo" : {
      "type" : "object",
      "required" : [ "channelId", "inpAlignment", "outAlignment", "sink", "source" ],
      "properties" : {
        "source" : {
          "type" : "string",
          "description" : "Identifier representing the source of the channel, i.e., Apache Kafka topicfrom which IPSM reads the RDF data to be translated."
        },
        "inpAlignment" : {
          "type" : "string",
          "description" : "Identifier of the input alignment, used for translating the incoming RDF data."
        },
        "outAlignment" : {
          "type" : "string",
          "description" : "Identifier of the output alignment, used for translating the outgoing RDF data."
        },
        "sink" : {
          "type" : "string",
          "description" : "Identifier of the sink of the channel, i.e., Apache Kafka topic to which IPSM publishes translated RDF data."
        },
        "channelId" : {
          "type" : "string",
          "description" : "UUID of the channel."
        }
      }
    },
    "ChannelConfig" : {
      "type" : "object",
      "required" : [ "inpAlignment", "outAlignment", "sink", "source" ],
      "properties" : {
        "source" : {
          "type" : "string",
          "description" : "Identifier representing the source of the channel, i.e., Apache Kafka topicfrom which IPSM reads the RDF data to be translated."
        },
        "inpAlignment" : {
          "type" : "string",
          "description" : "Identifier of the input alignment, used for translating the incoming RDF data."
        },
        "outAlignment" : {
          "type" : "string",
          "description" : "Identifier of the output alignment, used for translating the outgoing RDF data."
        },
        "sink" : {
          "type" : "string",
          "description" : "Identifier of the sink of the channel, i.e., Apache Kafka topic to which IPSM publishes translated RDF data."
        }
      }
    },
    "Alignment" : {
      "type" : "object",
      "required" : [ "inpAlignment", "outAlignment" ],
      "properties" : {
        "inpAlignment" : {
          "type" : "string",
          "description" : "Identifier of the input alignment, used for translating the incoming RDF data."
        },
        "outAlignment" : {
          "type" : "string",
          "description" : "Identifier of the output alignment, used for translating the outgoing RDF data."
        }
      }
    },
    "Error" : {
      "type" : "object",
      "required" : [ "message" ],
      "properties" : {
        "message" : {
          "type" : "string"
        }
      }
    },
    "Response" : {
      "type" : "object",
      "required" : [ "message" ],
      "properties" : {
        "message" : {
          "type" : "string"
        }
      }
    }
  }
}
package eu.interiot.ipsm.workshop

import org.rogach.scallop.ValueConverter

object HostSpec {
  def default = HostSpec("127.0.0.1", 9092)
  val hostnameConverter = new ValueConverter[HostSpec] {
    val nameRgx = """([a-z0-9\.]+):(\d+)""".r
    // parse is a method, that takes a list of arguments to all option invokations:
    // for example, "-a 1 2 -a 3 4 5" would produce List(List(1,2),List(3,4,5)).
    // parse returns Left with error message, if there was an error while parsing
    // if no option was found, it returns Right(None)
    // and if option was found, it returns Right(...)
    def parse(s:List[(String, List[String])]): Either[String, Option[HostSpec]] = s match {
      case (_, nameRgx(hname, port) :: Nil) :: Nil =>
        Right(Some(HostSpec(hname, port.toInt)))
      case Nil =>
        Right(None)
      case _ =>
        Left("Provide a valid Kafka server hostname and port")
    }

    val tag = scala.reflect.runtime.universe.typeTag[HostSpec] // some magic to make typing work
    val argType = org.rogach.scallop.ArgType.SINGLE
  }
}

case class HostSpec(hostname: String, port: Int) {
  override def toString: String = s"$hostname:$port"
}

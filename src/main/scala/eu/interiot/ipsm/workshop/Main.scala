package eu.interiot.ipsm.workshop

import akka.actor.{ActorSystem, Props}
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import com.typesafe.config.ConfigFactory
import com.typesafe.scalalogging.LazyLogging

import scala.util.{Failure, Success}
import org.rogach.scallop._

object Main extends App with RestApi with LazyLogging {
  implicit val system = ActorSystem()
  implicit val materializer = ActorMaterializer()
  implicit val executionContext = system.dispatcher

  val config = ConfigFactory.load()
  val host = config.getString("http.host")
  val port = config.getInt("http.port")

  class Conf(arguments: Seq[String]) extends ScallopConf(arguments) {
    val bootstrap = opt[HostSpec]()(HostSpec.hostnameConverter)
    verify()
  }

  val params = new Conf(args)

  Http().bindAndHandle(handler = api, interface = host, port = port) onComplete {
    case Success(binding) =>
      logger.info(s"REST interface bound to http:/${binding.localAddress}")
      system.actorOf(Props(new Reaper(binding)), name = "reaper")
    case Failure(ex) =>
      logger.error(s"REST interface could not bind to $host:$port", ex.getMessage)
  }
}

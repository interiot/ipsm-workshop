package eu.interiot.ipsm.workshop.alignments

import org.w3.banana.RDF

trait AlignmentCellTrait[Rdf <: RDF] {

  def askCell(graph: Rdf#Graph): Boolean

  def applyCell(graph: Rdf#Graph): Rdf#Graph

}

class AlignmentCell[Rdf <: RDF](askFunction: (Rdf#Graph) => Boolean, applyFunction: (Rdf#Graph) => Rdf#Graph)
  extends AlignmentCellTrait[Rdf] {

  override def askCell(graph: Rdf#Graph): Boolean = askFunction(graph)

  override def applyCell(graph: Rdf#Graph): Rdf#Graph = applyFunction(graph)

}
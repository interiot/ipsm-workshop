package eu.interiot.ipsm.workshop.alignments

import com.typesafe.scalalogging.LazyLogging
import org.w3.banana.{RDF, RDFOps, SparqlEngine, SparqlOps}
import org.w3.banana.jena.Jena
import org.w3.banana.io._

import scala.util.Try


object HardSPARQLJena extends HardSPARQL[Jena]

class HardSPARQL[Rdf <: RDF](
                              implicit ops: RDFOps[Rdf],
                              reader: RDFReader[Rdf, Try, RDFXML],
                              writer: RDFWriter[Rdf, Try, RDFXML],
                              n3Writer: RDFWriter[Rdf, Try, N3],
                              turtleWriter: RDFWriter[Rdf, Try, Turtle],
                              sparqlOperations: SparqlOps[Rdf],
                              sparqlGraph: SparqlEngine[Rdf, Try, Rdf#Graph]
                            )
  extends LazyLogging {

  import ops._
  import sparqlOperations._

  val sparqlHelper = new SparqlQueriesHelper[Rdf](false)

  val debugNames = Map(
    "1" -> s"alignment 1 (geoRSS)",
    "2" -> s"alignment 2 (Lat-Long)",
    "3" -> s"alignment 3 (structure)",
    "4" -> s"alignment 4 (function)"
  )

  def askAlignmentCell1(graph: Rdf#Graph): Boolean = {
    val debugName = debugNames("1")
    val queryAsk =
      """
        |ASK {
        | ?x georss:point ?z .
        | FILTER ( datatype(?z) = <http://www.w3.org/2001/XMLSchema#string> )
        | }
      """.stripMargin

    logger.debug(s"Asking about $debugName...\n*****")
    logger.debug(s"[$debugName]Ask query: $queryAsk")
    val result = sparqlHelper.doAsk(graph, queryAsk)
    logger.debug(s"[$debugName] Ask query result: $result")
    result
  }

  def applyAlignmentCell1(graph: Rdf#Graph): Rdf#Graph = {

    val debugName = debugNames("1")
    logger.debug(s"\nApplying $debugName...\n*****")

    //TODO: Make this better (throw an error, test this function for multiple white spaces etc.)
    def getValsFromGEORSS(geoRSSStr: String): (String, String) = {
      val tokens = geoRSSStr.split(" ").map(_.trim)
      if (tokens.length < 2) {
        logger.warn(s"[$debugName] Transformation input did not parse well: $geoRSSStr")
        ("", "")
      } else {
        (tokens(0).stripPrefix("\"").stripSuffix("\"").trim, tokens(1).stripPrefix("\"").stripSuffix("\"").trim)
      }
    }

    //Copy whole input graph to a triple set
    var tripleSet = graph.triples.toSet[Rdf#Triple]
    logger.debug(s"[$debugName] input triple set: $tripleSet")

    val varNames = ("x", "point")

    val constructQueryInput =
      s"""
         |CONSTRUCT {
         | ?${varNames._1} georss:point ?${varNames._2} .
         |} WHERE {
         | ?${varNames._1} georss:point ?${varNames._2} .
         | FILTER ( datatype(?${varNames._2}) = <http://www.w3.org/2001/XMLSchema#string> )
         |}
      """.stripMargin

    logger.debug(s"[$debugName] relevant input construct query: $constructQueryInput")
    val constructResultInput = sparqlHelper.doConstruct(graph, constructQueryInput)
    logger.debug(s"[$debugName] relevant input construct query result: $constructResultInput")

    val querySelect =
      s"""
         |SELECT ?${varNames._1} ?${varNames._2} WHERE {
         | ?${varNames._1} georss:point ?${varNames._2} .
         | FILTER ( datatype(?${varNames._2}) = <http://www.w3.org/2001/XMLSchema#string> )
         |}
      """.stripMargin

    logger.debug(s"[$debugName] relevant input select query: $querySelect")
    val selectResult = sparqlHelper.doSelect(graph, querySelect)
    logger.debug(s"[$debugName] relevant input select query result: $selectResult")

    //Remove "input" triples from the set
    tripleSet = tripleSet -- constructResultInput.triples
    logger.debug(s"[$debugName] triples without relevant input: $tripleSet")

    for (solution <- selectResult.iterator()) {
      val xNode = solution(s"${varNames._1}").get
      val x = if (xNode.isBNode) "_:b" else s"<${xNode.toString}>"

      val geoRSSPointStr = solution(s"${varNames._2}").get.as[Rdf#Literal].get.lexicalForm
      val (lat, long) = getValsFromGEORSS(geoRSSPointStr)
      val latLongDatatypeStr = "http://www.w3.org/2001/XMLSchema#decimal"

      val constructQueryOutput =
        s"""
           |CONSTRUCT {
           |    $x wgs84_pos:lat  "$lat"^^<$latLongDatatypeStr> ;
           |       wgs84_pos:long "$long"^^<$latLongDatatypeStr> .
           |} WHERE {
           |    $x georss:point "$geoRSSPointStr"^^<http://www.w3.org/2001/XMLSchema#string> .
           |}
        """.stripMargin

      logger.debug(s"[$debugName] output construct query: $constructQueryOutput")
      val constructResultOutput = sparqlHelper.doConstruct(graph, constructQueryOutput)
      logger.debug(s"[$debugName] output construct query result: $constructResultOutput")

      //Add "output" triples to the set
      tripleSet = tripleSet ++ constructResultOutput.triples
      logger.debug(s"[$debugName] triple set with output triples: $tripleSet")
    }

    val outputGraph = Graph(tripleSet)
    //n3Writer.write(outputGraph, new FileOutputStream(outputFilePaths("5")), "http://foo.com")

    logger.debug(s"[$debugName] output graph: $outputGraph")
    logger.debug(s"... finished applying $debugName")
    outputGraph
  }

  def askAlignmentCell21(graph: Rdf#Graph): Boolean ={
    val debugName = debugNames("2")
    val queryAsk =
      """
        |ASK {
        | ?x my_port:hasGPSlongitude ?z .
        | }
      """.stripMargin

    logger.debug(s"Asking about ${debugName}...\n*****")
    logger.debug(s"[${debugName}]Ask query: ${queryAsk}")
    val result = sparqlHelper.doAsk(graph, queryAsk )
    logger.debug(s"[${debugName}] Ask query result: ${result}")
    result
  }

  def askAlignmentCell22(graph: Rdf#Graph): Boolean ={
    val debugName = debugNames("2")
    val queryAsk =
      """
        |ASK {
        | ?x my_port:hasGPSlongitude ?w .
        | }
      """.stripMargin

    logger.debug(s"Asking about ${debugName}...\n*****")
    logger.debug(s"[${debugName}]Ask query: ${queryAsk}")
    val result = sparqlHelper.doAsk(graph, queryAsk )
    logger.debug(s"[${debugName}] Ask query result: ${result}")
    result
  }

  /**
  Device
	|_GPS position (xsd:string) + multiple other parameters
	    ===>
	Device
	|_GPS longitude (xsd:decimal)
	|_GPS latitude (xsd:decimal)
    */
  def applyAlignmentCell21(graph: Rdf#Graph): Rdf#Graph ={
    val debugName = debugNames("2")
    logger.debug(s"\nApplying ${debugName}...\n*****")

    //Copy whole input graph to a triple set
    var tripleSet = graph.triples.toSet[Rdf#Triple]
    logger.debug(s"[${debugName}] input triple set: ${tripleSet}")

    val varNames = ("x", "latitude", "longitude")

    val constructQueryInput =
      s"""
         |CONSTRUCT {
         | ?${varNames._1} my_port:hasGPSlongitude ?${varNames._3}.
         |} WHERE {
         | ?${varNames._1} my_port:hasGPSlongitude ?${varNames._3} .
         |}
      """.stripMargin

    logger.debug(s"[${debugName}] relevant input construct query: ${constructQueryInput}")
    val constructResultInput = sparqlHelper.doConstruct(graph, constructQueryInput)
    logger.debug(s"[${debugName}] relevant input construct query result: ${constructResultInput}")


    //Remove "input" triples from the set
    tripleSet = tripleSet -- constructResultInput.triples
    logger.debug(s"[${debugName}] triples without relevant input: ${tripleSet}")

    val constructQueryOutput =
      s"""
         |CONSTRUCT {
         | ?${varNames._1} wgs84_pos:long ?${varNames._3}.
         |} WHERE {
         |  ?${varNames._1} my_port:hasGPSlongitude ?${varNames._3} .
         |}
      """.stripMargin

    logger.debug(s"[${debugName}] output construct query: ${constructQueryOutput}")
    val constructResultOutput = sparqlHelper.doConstruct(graph, constructQueryOutput)
    logger.debug(s"[${debugName}] output construct query result: ${constructResultOutput}")

    //Add "output" triples to the set
    tripleSet = tripleSet ++ constructResultOutput.triples
    logger.debug(s"[${debugName}] triple set with output triples: ${tripleSet}")

    val outputGraph = Graph(tripleSet)

    logger.debug(s"[${debugName}] output graph: ${outputGraph}")
    logger.debug(s"... finished applying ${debugName}")
    outputGraph
  }

  def applyAlignmentCell22(graph: Rdf#Graph): Rdf#Graph ={
    val debugName = debugNames("2")
    logger.debug(s"\nApplying ${debugName}...\n*****")

    //Copy whole input graph to a triple set
    var tripleSet = graph.triples.toSet[Rdf#Triple]
    logger.debug(s"[${debugName}] input triple set: ${tripleSet}")

    val varNames = ("x", "latitude", "longitude")

    val constructQueryInput =
      s"""
         |CONSTRUCT {
         | ?${varNames._1} my_port:hasGPSlatitude ?${varNames._2} .
         |} WHERE {
         |  {?${varNames._1} my_port:hasGPSlatitude ?${varNames._2} .
         |}
      """.stripMargin

    logger.debug(s"[${debugName}] relevant input construct query: ${constructQueryInput}")
    val constructResultInput = sparqlHelper.doConstruct(graph, constructQueryInput)
    logger.debug(s"[${debugName}] relevant input construct query result: ${constructResultInput}")


    //Remove "input" triples from the set
    tripleSet = tripleSet -- constructResultInput.triples
    logger.debug(s"[${debugName}] triples without relevant input: ${tripleSet}")

    val constructQueryOutput =
      s"""
         |CONSTRUCT {
         | ?${varNames._1} wgs84_pos:lat ?${varNames._2} .
         |} WHERE {
         | ?${varNames._1} my_port:hasGPSlatitude ?${varNames._2} .
         |}
      """.stripMargin

    logger.debug(s"[${debugName}] output construct query: ${constructQueryOutput}")
    val constructResultOutput = sparqlHelper.doConstruct(graph, constructQueryOutput)
    logger.debug(s"[${debugName}] output construct query result: ${constructResultOutput}")

    //Add "output" triples to the set
    tripleSet = tripleSet ++ constructResultOutput.triples
    logger.debug(s"[${debugName}] triple set with output triples: ${tripleSet}")

    val outputGraph = Graph(tripleSet)

    logger.debug(s"[${debugName}] output graph: ${outputGraph}")
    logger.debug(s"... finished applying ${debugName}")
    outputGraph
  }

  def askAlignmentCell31(graph: Rdf#Graph): Boolean ={
    val debugName = debugNames("3")
    val queryAsk =
      """
        |ASK {
        | ?x ont3:hasObservationPrecision ?z .
        | }
      """.stripMargin

    logger.debug(s"Asking about ${debugName}...\n*****")
    logger.debug(s"[${debugName}]Ask query: ${queryAsk}")
    val result = sparqlHelper.doAsk(graph, queryAsk )
    logger.debug(s"[${debugName}] Ask query result: ${result}")
    result
  }

  def askAlignmentCell32(graph: Rdf#Graph): Boolean ={
    val debugName = debugNames("3")
    val queryAsk =
      """
        |ASK {
        | ?x ont3:hasOwner ?w .
        |  ?x ont3:hasOwnerInstitution ?v .
        | }
      """.stripMargin

    logger.debug(s"Asking about ${debugName}...\n*****")
    logger.debug(s"[${debugName}]Ask query: ${queryAsk}")
    val result = sparqlHelper.doAsk(graph, queryAsk )
    logger.debug(s"[${debugName}] Ask query result: ${result}")
    result
  }

  /**
  Device
	|_hasObservationPrecision (xsd:decimal)
	|_hasOwner
	|_hasOwnerInstitution

	   ===>

	Device
	|_hasOwner
		|_hasInstitution
	|_observes
		|_observableProperty
			|_hasPrecision (xsd:decimal)
			|_hasFrequency (NEEDS TO BE EMPTY)
    */
  def applyAlignmentCell31(graph: Rdf#Graph): Rdf#Graph ={
    val debugName = debugNames("3")
    logger.debug(s"\nApplying ${debugName}...\n*****")

    //Copy whole input graph to a triple set
    var tripleSet = graph.triples.toSet[Rdf#Triple]
    logger.debug(s"[${debugName}] input triple set: ${tripleSet}")

    val varNames = ("x", "observationPrecision", "owner", "ownerInstitution")

    val constructQueryInput =
      s"""
         |CONSTRUCT {
         | ?${varNames._1} ont3:hasObservationPrecision ?${varNames._2} .
         |} WHERE {
         | ?${varNames._1} ont3:hasObservationPrecision ?${varNames._2} .
         |}
      """.stripMargin

    logger.debug(s"[${debugName}] relevant input construct query: ${constructQueryInput}")
    val constructResultInput = sparqlHelper.doConstruct(graph, constructQueryInput)
    logger.debug(s"[${debugName}] relevant input construct query result: ${constructResultInput}")


    //Remove "input" triples from the set
    tripleSet = tripleSet -- constructResultInput.triples
    logger.debug(s"[${debugName}] triples without relevant input: ${tripleSet}")

    //TODO: This produces an "empty" observation, where it should not (look at output for AlignmentResources/Inputs/3.rdf)
    val constructQueryOutput =
    s"""
       |CONSTRUCT {
       | ?${varNames._1} ont5:observes _:b0 .
       |  _:b0 a ont5:ObservableProperty ;
       |      ont5:hasPrecision ?${varNames._2} ;
       |      ont5:hasFrequency "" ;
       |} WHERE {
       | ?${varNames._1} ont3:hasObservationPrecision ?${varNames._2} .
       |}
      """.stripMargin

    logger.debug(s"[${debugName}] output construct query: ${constructQueryOutput}")
    val constructResultOutput = sparqlHelper.doConstruct(graph, constructQueryOutput)
    logger.debug(s"[${debugName}] output construct query result: ${constructResultOutput}")

    //Add "output" triples to the set
    tripleSet = tripleSet ++ constructResultOutput.triples
    logger.debug(s"[${debugName}] triple set with output triples: ${tripleSet}")

    val outputGraph = Graph(tripleSet)

    logger.debug(s"[${debugName}] output graph: ${outputGraph}")
    logger.debug(s"... finished applying ${debugName}")
    outputGraph
  }

  def applyAlignmentCell32(graph: Rdf#Graph): Rdf#Graph ={
    val debugName = debugNames("3")
    logger.debug(s"\nApplying ${debugName}...\n*****")

    //Copy whole input graph to a triple set
    var tripleSet = graph.triples.toSet[Rdf#Triple]
    logger.debug(s"[${debugName}] input triple set: ${tripleSet}")

    val varNames = ("x", "observationPrecision", "owner", "ownerInstitution")

    val constructQueryInput =
      s"""
         |CONSTRUCT {
         | ?${varNames._1} ont3:hasOwner ?${varNames._3} ;
         |                 ont3:hasOwnerInstitution ?${varNames._4} .
         |} WHERE {
         | ?${varNames._1} ont3:hasOwner ?${varNames._3} .
         | ?${varNames._1} ont3:hasOwnerInstitution ?${varNames._4} .
         |}
      """.stripMargin

    logger.debug(s"[${debugName}] relevant input construct query: ${constructQueryInput}")
    val constructResultInput = sparqlHelper.doConstruct(graph, constructQueryInput)
    logger.debug(s"[${debugName}] relevant input construct query result: ${constructResultInput}")


    //Remove "input" triples from the set
    tripleSet = tripleSet -- constructResultInput.triples
    logger.debug(s"[${debugName}] triples without relevant input: ${tripleSet}")

    //TODO: This produces an "empty" observation, where it should not (look at output for AlignmentResources/Inputs/3.rdf)
    val constructQueryOutput =
    s"""
       |CONSTRUCT {
       | ?${varNames._1} ont5:hasOwner ?${varNames._3} .
       | ?${varNames._3} ont5:hasInstitution ?${varNames._4} .
       |} WHERE {
       |  ?${varNames._1} ont3:hasOwner ?${varNames._3} .
       |  ?${varNames._1} ont3:hasOwnerInstitution ?${varNames._4} .
       |}
      """.stripMargin

    logger.debug(s"[${debugName}] output construct query: ${constructQueryOutput}")
    val constructResultOutput = sparqlHelper.doConstruct(graph, constructQueryOutput)
    logger.debug(s"[${debugName}] output construct query result: ${constructResultOutput}")

    //Add "output" triples to the set
    tripleSet = tripleSet ++ constructResultOutput.triples
    logger.debug(s"[${debugName}] triple set with output triples: ${tripleSet}")

    val outputGraph = Graph(tripleSet)

    logger.debug(s"[${debugName}] output graph: ${outputGraph}")
    logger.debug(s"... finished applying ${debugName}")
    outputGraph
  }

  def askAlignmentCell41(graph: Rdf#Graph): Boolean ={
    val debugName = debugNames("4")
    val queryAsk =
      """
        |ASK {
        | ?x ont3:hasFunction ont3:openCloseFunction .
        | }
      """.stripMargin

    logger.debug(s"Asking about ${debugName}...\n*****")
    logger.debug(s"[${debugName}]Ask query: ${queryAsk}")
    val result = sparqlHelper.doAsk(graph, queryAsk )
    logger.debug(s"[${debugName}] Ask query result: ${result}")
    result
  }

  def askAlignmentCell42(graph: Rdf#Graph): Boolean ={
    val debugName = debugNames("4")
    val queryAsk =
      """
        |ASK {
        | ?x ont3:hasFunction ont3:tagFunction .
        | }
      """.stripMargin

    logger.debug(s"Asking about ${debugName}...\n*****")
    logger.debug(s"[${debugName}]Ask query: ${queryAsk}")
    val result = sparqlHelper.doAsk(graph, queryAsk )
    logger.debug(s"[${debugName}] Ask query result: ${result}")
    result
  }

  def askAlignmentCell43(graph: Rdf#Graph): Boolean ={
    val debugName = debugNames("4")
    val queryAsk =
      """
        |ASK {
        | ?x ont3:hasFunction ont3:measureFunction .
        | }
      """.stripMargin

    logger.debug(s"Asking about ${debugName}...\n*****")
    logger.debug(s"[${debugName}]Ask query: ${queryAsk}")
    val result = sparqlHelper.doAsk(graph, queryAsk )
    logger.debug(s"[${debugName}] Ask query result: ${result}")
    result
  }

  /**
  Device
    |_hasFunction openCloseFunction
    	  ===>
  Device
    |_machtBetrieb openFunction
    |_machtBetrieb closeFunction
    */
  def applyAlignmentCell41(graph: Rdf#Graph): Rdf#Graph ={
    val debugName = debugNames("4")
    logger.debug(s"\nApplying ${debugName}...\n*****")

    //Copy whole input graph to a triple set
    var tripleSet = graph.triples.toSet[Rdf#Triple]
    logger.debug(s"[${debugName}] input triple set: ${tripleSet}")

    val varName = ("x")

    val constructQueryInput =
      s"""
         |CONSTRUCT {
         | ?${varName} ont3:hasFunction ont3:openCloseFunction .
         |} WHERE {
         | ?${varName} ont3:hasFunction ont3:openCloseFunction .
         |}
      """.stripMargin

    logger.debug(s"[${debugName}] relevant input construct query: ${constructQueryInput}")
    val constructResultInput = sparqlHelper.doConstruct(graph, constructQueryInput)
    logger.debug(s"[${debugName}] relevant input construct query result: ${constructResultInput}")


    //Remove "input" triples from the set
    tripleSet = tripleSet -- constructResultInput.triples
    logger.debug(s"[${debugName}] triples without relevant input: ${tripleSet}")

    val constructQueryOutput =
      s"""
         |CONSTRUCT {
         | ?${varName} ont6:machtBetrieb ont6:openFunction , ont6:closeFunction .
         |} WHERE {
         | ?${varName} ont3:hasFunction ont3:openCloseFunction .
         |}
      """.stripMargin

    logger.debug(s"[${debugName}] output construct query: ${constructQueryOutput}")
    val constructResultOutput = sparqlHelper.doConstruct(graph, constructQueryOutput)
    logger.debug(s"[${debugName}] output construct query result: ${constructResultOutput}")

    //Add "output" triples to the set
    tripleSet = tripleSet ++ constructResultOutput.triples
    logger.debug(s"[${debugName}] triple set with output triples: ${tripleSet}")

    val outputGraph = Graph(tripleSet)

    logger.debug(s"[${debugName}] output graph: ${outputGraph}")
    logger.debug(s"... finished applying ${debugName}")
    outputGraph
  }

  def applyAlignmentCell42(graph: Rdf#Graph): Rdf#Graph ={
    val debugName = debugNames("4")
    logger.debug(s"\nApplying ${debugName}...\n*****")

    //Copy whole input graph to a triple set
    var tripleSet = graph.triples.toSet[Rdf#Triple]
    logger.debug(s"[${debugName}] input triple set: ${tripleSet}")

    val varName = ("x")

    val constructQueryInput =
      s"""
         |CONSTRUCT {
         | ?${varName} ont3:hasFunction ont3:tagFunction .
         |} WHERE {
         | ?${varName} ont3:hasFunction ont3:tagFunction .
         |}
      """.stripMargin

    logger.debug(s"[${debugName}] relevant input construct query: ${constructQueryInput}")
    val constructResultInput = sparqlHelper.doConstruct(graph, constructQueryInput)
    logger.debug(s"[${debugName}] relevant input construct query result: ${constructResultInput}")


    //Remove "input" triples from the set
    tripleSet = tripleSet -- constructResultInput.triples
    logger.debug(s"[${debugName}] triples without relevant input: ${tripleSet}")

    val constructQueryOutput =
      s"""
         |CONSTRUCT {
         | ?${varName} ont6:machtBetrieb ont6:tagFunction .
         |} WHERE {
         | ?${varName} ont3:hasFunction ont3:tagFunction .
         |}
      """.stripMargin

    logger.debug(s"[${debugName}] output construct query: ${constructQueryOutput}")
    val constructResultOutput = sparqlHelper.doConstruct(graph, constructQueryOutput)
    logger.debug(s"[${debugName}] output construct query result: ${constructResultOutput}")

    //Add "output" triples to the set
    tripleSet = tripleSet ++ constructResultOutput.triples
    logger.debug(s"[${debugName}] triple set with output triples: ${tripleSet}")

    val outputGraph = Graph(tripleSet)

    logger.debug(s"[${debugName}] output graph: ${outputGraph}")
    logger.debug(s"... finished applying ${debugName}")
    outputGraph
  }

  def applyAlignmentCell43(graph: Rdf#Graph): Rdf#Graph ={
    val debugName = debugNames("4")
    logger.debug(s"\nApplying ${debugName}...\n*****")

    //Copy whole input graph to a triple set
    var tripleSet = graph.triples.toSet[Rdf#Triple]
    logger.debug(s"[${debugName}] input triple set: ${tripleSet}")

    val varName = ("x")

    val constructQueryInput =
      s"""
         |CONSTRUCT {
         | ?${varName} ont3:hasFunction ont3:measureFunction .
         |} WHERE {
         | ?${varName} ont3:hasFunction ont3:measureFunction .
         |}
      """.stripMargin

    logger.debug(s"[${debugName}] relevant input construct query: ${constructQueryInput}")
    val constructResultInput = sparqlHelper.doConstruct(graph, constructQueryInput)
    logger.debug(s"[${debugName}] relevant input construct query result: ${constructResultInput}")


    //Remove "input" triples from the set
    tripleSet = tripleSet -- constructResultInput.triples
    logger.debug(s"[${debugName}] triples without relevant input: ${tripleSet}")

    val constructQueryOutput =
      s"""
         |CONSTRUCT {
         | ?${varName} ont6:machtBetrieb ont6:measureFunction .
         |} WHERE {
         | ?${varName} ont3:hasFunction ont3:measureFunction .
         |}
      """.stripMargin

    logger.debug(s"[${debugName}] output construct query: ${constructQueryOutput}")
    val constructResultOutput = sparqlHelper.doConstruct(graph, constructQueryOutput)
    logger.debug(s"[${debugName}] output construct query result: ${constructResultOutput}")

    //Add "output" triples to the set
    tripleSet = tripleSet ++ constructResultOutput.triples
    logger.debug(s"[${debugName}] triple set with output triples: ${tripleSet}")

    val outputGraph = Graph(tripleSet)

    logger.debug(s"[${debugName}] output graph: ${outputGraph}")
    logger.debug(s"... finished applying ${debugName}")
    outputGraph
  }
}

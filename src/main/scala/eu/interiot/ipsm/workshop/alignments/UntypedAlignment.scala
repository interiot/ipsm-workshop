package eu.interiot.ipsm.workshop.alignments

import org.w3.banana.{RDF, RDFOps}

class UntypedAlignment[Rdf <: RDF](implicit ops: RDFOps[Rdf]) {

  import ops._

  var alignmentCells = Set[AlignmentCellTrait[Rdf]]()

  //  def this(type1: String, type2: String, cells: List[SparqlDemoAlignmentCell[Rdf]]) = {
  //    val t = this[Rdf](type1, type2)
  //  }

  //def getType(): (String,String) = (inputAlignment, outputAlignment)

  //  def fitsType(inputType: String, outputType: String): Boolean = {
  //    inputType == inputAlignment && outputType == outputAlignment
  //  }

  //  def fitsType(types: (String, String)): Boolean ={
  //    fitsType(types._1, types._2)
  //  }

  def addCell(cell: AlignmentCellTrait[Rdf]): Unit = {
    alignmentCells = alignmentCells + cell
  }

  def applyCells(graph: Rdf#Graph): Rdf#Graph = {
    var outputGraph = graph.copy
    var notEveryCellChecked = true
    while (notEveryCellChecked) {
      notEveryCellChecked = false
      for (cell <- alignmentCells) {
        if (cell.askCell(outputGraph)) {
          outputGraph = cell.applyCell(outputGraph)
          notEveryCellChecked = true
        }
      }
    }
    outputGraph
  }
}

package eu.interiot.ipsm.workshop.alignments

import java.io._

import com.typesafe.scalalogging.LazyLogging
import org.w3.banana.io.{Turtle, _}
import org.w3.banana.jena.Jena
import org.w3.banana.{RDF, RDFOps, SparqlEngine, SparqlOps}

import scala.util.Try

object RDFHelperJena extends RDFHelper[Jena]

class RDFHelper[Rdf <: RDF](
                             implicit ops: RDFOps[Rdf],
                             XMLreader: RDFReader[Rdf, Try, RDFXML],
                             N3Reader: RDFReader[Rdf, Try, N3],
                             turtleReader: RDFReader[Rdf, Try, Turtle],
                             XMLwriter: RDFWriter[Rdf, Try, RDFXML],
                             N3Writer: RDFWriter[Rdf, Try, N3],
                             turtleWriter: RDFWriter[Rdf, Try, Turtle],
                             sparqlOperations: SparqlOps[Rdf],
                             sparqlGraph: SparqlEngine[Rdf, Try, Rdf#Graph]
                           )
  extends LazyLogging {

  val graphURI = "http://example.ipsm.sripas.org"

  def readGraphFromStringTurtle(graphStr: String): Try[Rdf#Graph] = {
    turtleReader.read(new ByteArrayInputStream(graphStr.getBytes), graphURI)
  }

  def readGraphFromStringN3(graphStr: String): Try[Rdf#Graph] = {
    N3Reader.read(new ByteArrayInputStream(graphStr.getBytes), graphURI)
  }

  def readGraphFromStringXML(graphStr: String): Try[Rdf#Graph] = {
    XMLreader.read(new ByteArrayInputStream(graphStr.getBytes), graphURI)
  }

  def readGraphFromString(graphStr: String, reader: RDFReader[Rdf, Try, _]): Try[Rdf#Graph] = {
    reader.read(new ByteArrayInputStream(graphStr.getBytes), graphURI)
  }

  def readGraphFromStreamTurtle(stream: InputStream): Try[Rdf#Graph] = {
    readGraphFromStream(stream, turtleReader)
  }

  def readGraphFromStreamN3(stream: InputStream): Try[Rdf#Graph] = {
    readGraphFromStream(stream, N3Reader)
  }

  def readGraphFromStreamXML(stream: InputStream): Try[Rdf#Graph] = {
    readGraphFromStream(stream, XMLreader)
  }

  def readGraphFromStream(stream: InputStream, reader: RDFReader[Rdf, Try, _]): Try[Rdf#Graph] = {
    reader.read(stream, graphURI)
  }

  def writeGraphToStreamTurtle(graph: Rdf#Graph, stream: OutputStream): Unit = {
    writeGraphToStream(graph, stream, turtleWriter)
  }

  def writeGraphToFileTurtle(graph: Rdf#Graph, path: String): Unit = {
    writeGraphToFile(graph, path, turtleWriter)
  }

  def writeGraphToStreamN3(graph: Rdf#Graph, stream: OutputStream): Unit = {
    writeGraphToStream(graph, stream, N3Writer)
  }

  def writeGraphToFileN3(graph: Rdf#Graph, path: String): Unit = {
    writeGraphToFile(graph, path, N3Writer)
  }

  def writeGraphToStreamXML(graph: Rdf#Graph, stream: OutputStream): Unit = {
    writeGraphToStream(graph, stream, XMLwriter)
  }

  def writeGraphToFileXML(graph: Rdf#Graph, path: String): Unit = {
    writeGraphToFile(graph, path, XMLwriter)
  }

  def writeGraphToStream(graph: Rdf#Graph, stream: OutputStream, writer: RDFWriter[Rdf, Try, _]): Unit = {
    logger.debug(s"Writing graph to stream at $stream")
    writer.write(graph, stream, graphURI)
  }

  def writeGraphToFile(graph: Rdf#Graph, path: String, writer: RDFWriter[Rdf, Try, _]): Unit = {
    writeGraphToStream(graph, new FileOutputStream(path), writer)
  }

  def writeGraphToStringTurtle(graph: Rdf#Graph): Try[String] = {
    writeGraphToString(graph, turtleWriter)
  }

  def writeGraphToStringN3(graph: Rdf#Graph): Try[String] = {
    writeGraphToString(graph, N3Writer)
  }

  def writeGraphToStringXML(graph: Rdf#Graph): Try[String] = {
    writeGraphToString(graph, XMLwriter)
  }

  def writeGraphToString(graph: Rdf#Graph, writer: RDFWriter[Rdf, Try, _]): Try[String] = {
    writer.asString(graph, graphURI)
  }
}

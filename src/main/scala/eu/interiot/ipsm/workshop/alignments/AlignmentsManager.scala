package eu.interiot.ipsm.workshop.alignments

import org.w3.banana.io.SparqlAnswerXml
import org.w3.banana.jena.Jena

object AlignmentsManager {

  val hardSparql = HardSPARQLJena

  val alignment1 = new UntypedAlignment[Jena]()
  alignment1.addCell(new AlignmentCell[Jena](hardSparql.askAlignmentCell1, hardSparql.applyAlignmentCell1))

  val alignment2 = new UntypedAlignment[Jena]()
  alignment2.addCell(new AlignmentCell[Jena](hardSparql.askAlignmentCell21, hardSparql.applyAlignmentCell21))
  alignment2.addCell(new AlignmentCell[Jena](hardSparql.askAlignmentCell22, hardSparql.applyAlignmentCell22))

  val alignment3 = new UntypedAlignment[Jena]()
  alignment3.addCell(new AlignmentCell[Jena](hardSparql.askAlignmentCell31, hardSparql.applyAlignmentCell31))
  alignment3.addCell(new AlignmentCell[Jena](hardSparql.askAlignmentCell32, hardSparql.applyAlignmentCell32))

  val alignment4 = new UntypedAlignment[Jena]()
  alignment4.addCell(new AlignmentCell[Jena](hardSparql.askAlignmentCell41, hardSparql.applyAlignmentCell41))
  alignment4.addCell(new AlignmentCell[Jena](hardSparql.askAlignmentCell42, hardSparql.applyAlignmentCell42))
  alignment4.addCell(new AlignmentCell[Jena](hardSparql.askAlignmentCell43, hardSparql.applyAlignmentCell43))

  val alignments = Map[(String, String), UntypedAlignment[Jena]](
    ("ont1", "ont4") -> alignment1,
    ("ont2", "ont4") -> alignment2,
    ("ont3", "ont5") -> alignment3,
    ("ont3", "ont6") -> alignment4
  )

  val allowedAlignments = alignments.keys.toSet[(String, String)]

  def isAlignmentAllowed(ont1: String, ont2: String): Boolean = {
    isAlignmentAllowed((ont1, ont2))
  }

  def isAlignmentAllowed(onts: (String, String)): Boolean = {
    alignments.keySet.contains(onts)
  }

  /**
    * Returns an alignment for a given pair of input/output ontologies
    * WARNING: Assumes that onts is a valid (allowed) input pair
    *
    * @param onts
    * @return
    */
  def getAlignment(onts: (String, String)): UntypedAlignment[Jena] = {
    alignments(onts)
  }
}
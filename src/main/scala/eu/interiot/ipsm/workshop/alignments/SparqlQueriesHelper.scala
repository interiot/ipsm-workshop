package eu.interiot.ipsm.workshop.alignments

import org.w3.banana._
import org.w3.banana.io.{RDFReader, RDFXML, SparqlQueryResultsReader, _}

import scala.util.Try
import com.typesafe.scalalogging.LazyLogging
import java.io._

/**
  * @param doLogging if true uses logger to log (and print) info messages
  * @param ops
  * @param sparqlOperations
  * @param sparqlGraph
  * @tparam Rdf
  */
class SparqlQueriesHelper[Rdf <: RDF](
                                       doLogging: Boolean
                                     )
                                     (
                                       implicit ops: RDFOps[Rdf],
                                       sparqlOperations: SparqlOps[Rdf],
                                       sparqlGraph: SparqlEngine[Rdf, Try, Rdf#Graph]
                                     )
  extends LazyLogging {

  import ops._
  import sparqlGraph.sparqlEngineSyntax._
  import sparqlOperations._

  /**
    * A string with a set of "standard" SPARQL prefixes (often used prefixes)
    */
  val prefixesMarginsStr =
  """prefix ont1: <http://www.example.org/ontology1#>
    |prefix ont2: <http://www.example.org/ontology2#>
    |prefix ont3: <http://www.example.org/ontology3#>
    |prefix ont4: <http://www.example.org/ontology4#>
    |prefix ont5: <http://www.example.org/ontology5#>
    |prefix ont6: <http://www.example.org/ontology6#>
    |prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    |prefix owl: <http://www.w3.org/2002/07/owl#>
    |prefix xsd: <http://www.w3.org/2001/XMLSchema#>
    |prefix iot-lite: <http://purl.oclc.org/NET/UNIS/fiware/iot-lite#>
    |prefix ns: <http://creativecommons.org/ns#>
    |prefix georss: <http://www.georss.org/georss/>
    |prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    |prefix OrientDB: <http://www.dewi.org/WDA.owl#OrientDB>
    |prefix ACS: <http://www.dewi.org/ACS.owl#>
    |prefix WDA: <http://www.dewi.org/WDA.owl#>
    |prefix terms: <http://purl.org/dc/terms/>
    |prefix xml: <http://www.w3.org/XML/1998/namespace#>
    |prefix wgs84_pos: <http://www.w3.org/2003/01/geo/wgs84_pos#>
    |prefix DUL: <http://www.loa-cnr.it/ontologies/DUL.owl#>
    |prefix foaf: <http://xmlns.com/foaf/0.1/>
    |prefix dc: <http://purl.org/dc/elements/1.1/>
    |prefix my_port: <http://example.sripas.org/ontologies/port.owl#>
  """

  /**
    * Adds a set of prefixes defined in the {@link prefixesMarginsStr} value
    *
    * @param query a string with a query with scala margins
    * @return a string with a sparql query with added prefixes and no margins
    */
  def addSparqlPrefixes(query: String): String = {
    (prefixesMarginsStr + query).stripMargin
  }

  def executeAndLogSelect(rdfGraph: Rdf#Graph, selectQuery: String): Rdf#Solutions = {
    val query = parseSelect(selectQuery).get
    val answers = rdfGraph.executeSelect(query).get
    if (doLogging) answers.toIterable.foreach(rdf => logger.info(s"${rdf.toString}"))
    answers
  }

  def executeAndLogAsk(rdfGraph: Rdf#Graph, askQuery: String): Boolean = {
    val query = parseAsk(askQuery).get
    val answer = rdfGraph.executeAsk(query).get
    if (doLogging) logger.info(s"${if (answer) "yes" else "no"}")
    answer
  }

  def executeAndLogConstruct(rdfGraph: Rdf#Graph, constructQuery: String): Rdf#Graph = {
    val query = parseConstruct(constructQuery).get
    val answer = rdfGraph.executeConstruct(query).get
    if (doLogging) logger.info(s"Constructed an rdf graph")
    answer
  }

  /**
    * Adds prefixes to a select query and executes it on a graph
    *
    * @param graph
    * @param select
    */
  def doSelect(graph: Rdf#Graph, select: String): Rdf#Solutions = {
    executeAndLogSelect(graph, addSparqlPrefixes(select))
  }

  /**
    * Adds prefixes to an ask query and executes it on a graph
    *
    * @param graph
    * @param ask
    */
  def doAsk(graph: Rdf#Graph, ask: String): Boolean = {
    executeAndLogAsk(graph, addSparqlPrefixes(ask))
  }

  /**
    * Adds prefixes to a construct query and executes it on a graph
    *
    * @param graph
    * @param construct
    */
  def doConstruct(graph: Rdf#Graph, construct: String): Rdf#Graph = {
    executeAndLogConstruct(graph, addSparqlPrefixes(construct))
  }

  //TODO: How to implement this
  def extractPrefixesFromGraph(rdfGraph: Rdf#Graph): String = ???
}

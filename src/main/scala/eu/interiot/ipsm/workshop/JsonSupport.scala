package eu.interiot.ipsm.workshop

import spray.json.DefaultJsonProtocol._

trait JsonSupport {
  implicit val errorFormat = jsonFormat1(ErrorResponse)
  implicit val responseFormat = jsonFormat1(SuccessResponse)
  implicit val alignmentPairFormat = jsonFormat2(AlignmentPair)
  implicit val channelConfigFormat = jsonFormat4(ChannelConfig)
  implicit val channelInfoFormat = jsonFormat5(ChannelInfo)
}

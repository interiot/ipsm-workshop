package eu.interiot.ipsm.workshop

import eu.interiot.ipsm.workshop.alignments._
import akka.actor.ActorSystem
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server._
import akka.kafka.scaladsl.Producer
import akka.kafka.{ProducerMessage, ProducerSettings}
import akka.stream.{ActorMaterializer, ActorMaterializerSettings, Supervision}
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.common.serialization.{ByteArraySerializer, StringSerializer}

import scala.concurrent.{ExecutionContext, Future}
import spray.json.DefaultJsonProtocol._
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import com.typesafe.scalalogging.LazyLogging

import scala.util.Try
import scala.util.control.NonFatal

trait Resources extends JsonSupport with LazyLogging {

  import akka.kafka.scaladsl.Consumer
  import akka.kafka.{ConsumerSettings, Subscriptions}
  import org.apache.kafka.clients.consumer.ConsumerConfig
  import org.apache.kafka.common.serialization.{ByteArrayDeserializer, StringDeserializer}

  import collection.mutable.{Map => MMap}

  private val channels: MMap[String, (ActorMaterializer, ChannelConfig)] = MMap()

  implicit val system: ActorSystem
  implicit val executionContext: ExecutionContext

  private def uuid() = java.util.UUID.randomUUID.toString

  private lazy val bootstrap: String = {
    val bs = Main.params.bootstrap.getOrElse(HostSpec.default)
    val hostname = s"${bs.hostname}:${bs.port}"
    logger.info(s"Kafka bootstrap server: $hostname")
    hostname
  }

  private val decider: Supervision.Decider = {
    case NonFatal(_) =>
      Supervision.Resume
    case _ =>
      Supervision.Stop
  }

  private def materializeChannel(chanConf: ChannelConfig, actorSystem: ActorSystem): Future[ActorMaterializer] = Future {
    val parallelism = 3
    val localMat = ActorMaterializer(ActorMaterializerSettings(actorSystem).withSupervisionStrategy(decider))(actorSystem)

    val producerSettings = ProducerSettings(system, new ByteArraySerializer, new StringSerializer)
      .withBootstrapServers(bootstrap)

    val consumerSettings = ConsumerSettings(system, new ByteArrayDeserializer, new StringDeserializer)
      .withBootstrapServers(bootstrap)
      .withGroupId(uuid()) // groups have to be uniuque
      .withProperty("dual.commit.enabled", "false")
      //  .withProperty("offsets.storage", "kafka")
      //  .withProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest")
      .withProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "latest")

    val alignment = AlignmentsManager.getAlignment(chanConf.inpAlignment, chanConf.outAlignment)

    Consumer.committableSource(consumerSettings, Subscriptions.topics(chanConf.source))
      .mapAsync(parallelism)(msg => {
        Future {
          val rec = msg.record
          println(s"topic: ${rec.topic()}, partition: ${rec.partition()}, value: ${rec.value()}, offset: ${rec.offset()}")
          msg
        }
      })
      .mapAsync(parallelism) { msg => Future {
        val result = for {
          // in: RDF/XML => Rdf#Graph
          inputGraph <- RDFHelperJena.readGraphFromStringXML(msg.record.value())
          // trans: Rdf#Graph => Rdf#Graph
          outputGraph <- Try(alignment.applyCells(inputGraph))
          // out: Rdf#Graph => RDF/XML
          outputGraphStr <- RDFHelperJena.writeGraphToStringXML(outputGraph)
        } yield {
          outputGraphStr
        }
        println(result)
        ProducerMessage.Message(new ProducerRecord[Array[Byte], String](
          chanConf.sink,
          result.get
        ), msg.committableOffset)
      }
      }
      .runWith(Producer.commitableSink(producerSettings))(localMat)

    localMat
  }

  def createChannel(chanConf: ChannelConfig): Route = {
    if (channels.values.exists(conf => conf._2.sink == chanConf.sink)) {
      complete(StatusCodes.BadRequest, ErrorResponse(
        s"""Channel with output ID="${chanConf.sink}" already exists"""
      ))
    } else {
      if (!AlignmentsManager.isAlignmentAllowed(chanConf.inpAlignment, chanConf.outAlignment)) {
        complete(StatusCodes.BadRequest, ErrorResponse(
          s"""Unsupported alignment configuration: (${chanConf.inpAlignment},${chanConf.outAlignment})"""
        ))
      } else {
        val saved: Future[ActorMaterializer] = materializeChannel(chanConf, system)
        onComplete(saved) { done =>
          val id = uuid()
          channels(id) = (done.get, chanConf)
          complete(StatusCodes.Created, SuccessResponse(message = s"Channel $id created successfully"))
        }
      }
    }
  }

  def deleteChannel(id: String): Route = channels.get(id) match {
    case Some((mat, _)) =>
      mat.shutdown()
      if (mat.isShutdown) {
        channels.remove(id)
        complete(StatusCodes.OK, SuccessResponse(s"Channel $id successfully closed"))
      } else {
        complete(StatusCodes.InternalServerError, ErrorResponse(
          s"""Closing channel "$id" FAILED"""
        ))
      }
    case None =>
      complete(StatusCodes.BadRequest, ErrorResponse(
        s"""Channel with identifier "$id" not found, already deleted?"""
      ))
  }

  def listChannels(): Route = {
    complete(StatusCodes.OK, for (p <- channels.toList) yield {
      val tc = p._2._2
      ChannelInfo(tc.source, tc.inpAlignment, tc.outAlignment, tc.sink, p._1)
    }
    )
  }

  def listAlignments(): Route = {
    complete(StatusCodes.OK, for (p <- AlignmentsManager.allowedAlignments) yield {
      AlignmentPair(p._1, p._2)
    }
    )
  }

}

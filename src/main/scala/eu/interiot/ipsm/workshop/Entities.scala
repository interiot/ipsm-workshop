package eu.interiot.ipsm.workshop

final case class ChannelConfig(source: String, inpAlignment: String, outAlignment: String, sink: String)
final case class ChannelInfo(source: String, inpAlignment: String, outAlignment: String, sink: String, channelId: String)
final case class SuccessResponse(message: String)
final case class ErrorResponse(message: String)
final case class AlignmentPair(inpAlignment: String, OutAlignment: String)


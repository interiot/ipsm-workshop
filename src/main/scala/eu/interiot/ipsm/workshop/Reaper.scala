package eu.interiot.ipsm.workshop

import akka.actor.Actor
import akka.http.scaladsl.Http.ServerBinding
import scala.concurrent.ExecutionContext.Implicits.global

/**
  * Created by wiesiek on 19/02/17.
  */
object Reaper {
  case object AllDone
}

class Reaper(binding: ServerBinding) extends Actor {
  import Reaper._
  def receive = {
    case AllDone =>
      binding.unbind() // trigger unbinding from the port
        .onComplete(_ ⇒ context.system.terminate()) // and shutdown when done
  }

}

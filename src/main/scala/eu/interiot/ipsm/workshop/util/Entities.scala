package eu.interiot.ipsm.workshop.util

import scala.util.Random

case class Person(firstName: String, lastName: String, id: Int)

case class XMLDevice(position: String, name: Option[String], description: Option[String])

case class CSVDevice(latitude: Float, longitude: Float, error: Float, satNum: Int, remark: String)

case class JSONDevice(name: String, precision: Float, owner: String, institution: String, functions: List[String])

object entitiesHelper {

  val randomGenerator = new Random()

  val namePool = List[String](
    "Darkwegud",
    "Dana",
    "Hagudushang",
    "Hareonsau",
    "Chiulfan",
    "Bene",
    "Ande",
    "Henazg",
    "Emagrim",
    "Vala",
    "Imilgrorn",
    "Genruul",
    "Ranemar",
    "Sanegash",
    "Ushangwa",
    "Sabmi",
    "Durai",
    "Barorn",
    "Masur",
    "Mazarg",
    "Peniel",
    "Ithuriel",
    "Sammael",
    "Appolyon",
    "Ezekiel",
    "Hermesiel",
    "Paschar",
    "Ithuriel",
    "Rathanael",
    "Temeluch",
    "Sensor1",
    "Sensor2",
    "Sensor3",
    "Sensor4",
    "Sensor5",
    "Sensor6"
  )

  val institutionsPool = List[String](
    "SRIPAS",
    "UPV",
    "CERN",
    "UNCLE",
    "NASA",
    "SpaceX"
  )

  val functionsPool = List[String](
    "openCloseFunction",
    "tagFunction",
    "measureFunction",
    "actuatingFunction"
  )

  def randomLatitude(): Float = (randomGenerator.nextFloat() * 180) - 90
  def randomLongitude(): Float = (randomGenerator.nextFloat() * 360) - 180
  def randomPositionStr(): String = s"${randomLatitude()} ${randomLongitude()}"
  def randomError(): Float = (math.floor((randomGenerator.nextFloat() * 10.2) * 10) / 10).toFloat
  def randomSatelliteNum(): Int = randomGenerator.nextInt(13)
  def randomPrecision(): Float = (math.floor((randomGenerator.nextFloat() * 10) * 10) / 10).toFloat
  def randomInstitution(): String = institutionsPool(randomGenerator.nextInt(institutionsPool.length))
  def randomName(): String = namePool(randomGenerator.nextInt(namePool.length))
  def randomFunctions(): List[String] = Random.shuffle(functionsPool).take(randomGenerator.nextInt(functionsPool.length - 1) + 1)

  def randomNameOption(): Option[String] = {
    if (randomGenerator.nextFloat < 0.5) {
      Option(randomName())
    } else {
      None
    }
  }

  def randomDescriptionOption(): Option[String] = {
    val random = randomGenerator.nextFloat < 0.3
    if (random) {
      Option("A device description")
    } else {
      None
    }
  }
}
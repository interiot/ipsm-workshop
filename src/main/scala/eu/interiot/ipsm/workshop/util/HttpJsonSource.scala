package eu.interiot.ipsm.workshop.util

import akka.NotUsed
import akka.http.scaladsl.common.{EntityStreamingSupport, JsonEntityStreamingSupport}
import akka.stream.scaladsl.{Flow, Source}
import akka.http.scaladsl.server.Directives._
import akka.util.ByteString
import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer

import scala.io.StdIn
import scala.util.{Failure, Success}
import com.typesafe.config.ConfigFactory
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import eu.interiot.ipsm.workshop.CorsSupport
import spray.json.{DefaultJsonProtocol, JsNumber, JsValue, JsonFormat}

object HttpJsonSource extends App with SprayJsonSupport with CorsSupport {

  implicit val system = ActorSystem()
  implicit val materializer = ActorMaterializer()
  implicit val executionContext = system.dispatcher

  object MyJsonProtocol extends DefaultJsonProtocol {
    implicit object MyFloatJsonFormat extends JsonFormat[Float] {
      def write(x: Float) = JsNumber(BigDecimal(x.toDouble).round(new java.math.MathContext(2)))
      def read(value: JsValue) = DefaultJsonProtocol.FloatJsonFormat.read(value)
    }
    implicit val personFormat = jsonFormat3(Person.apply)
    implicit val record2Json = jsonFormat5(JSONDevice.apply)
  }

  import MyJsonProtocol._

  val start = ByteString.empty
  val sep = ByteString("\n")
  val end = ByteString.empty

  implicit val jsonStreamingSupport: JsonEntityStreamingSupport =
    EntityStreamingSupport.json()
      .withFramingRenderer(Flow[ByteString].intersperse(start, sep, end))
      .withParallelMarshalling(parallelism = 8, unordered = false)

  /*
  "device": {
    "name": "Dev5",
    "precision": "10.0",
    "owner": "JohnDoe",
    "institution": "SRIPAS",
    "functions": ["openCloseFunction","actuatingFunction"]
  }
  */

  def sourceOfPeople(max: Int): Source[Person, NotUsed] = {
    Source(1 to max)
      .map { n =>
        println(s"Generating tweet No. $n")
        Person("John", "Doe", n)
      }
  }

  def sourceOfDevices(max: Int): Source[JSONDevice, NotUsed] = {
    Source(1 to max)
      .map { n =>
        JSONDevice(
          entitiesHelper.randomName(),
          entitiesHelper.randomPrecision(),
          s"owner${entitiesHelper.randomName()}",
          entitiesHelper.randomInstitution(),
          entitiesHelper.randomFunctions())
      }
  }

  val maxStreamLength: Int = 100000

  val route =
    corsHandler(
      path("json" / IntNumber) { n =>
        complete(sourceOfDevices(n))
      } ~
        path("json") {
          complete(sourceOfDevices(maxStreamLength))
        }
    )

  val config = ConfigFactory.load()
  val host = config.getString("json-http.host")
  val port = config.getInt("json-http.port")

  Http().bindAndHandle(handler = route, interface = host, port = port) onComplete {
    case Success(binding) =>
      println(s"JSON stream available from: http:/${binding.localAddress}/json[/<stream_length>]")
      println("Press <Enter> to STOP")
      StdIn.readLine() // let it run until user presses return
      binding.unbind() // trigger unbinding from the port
        .onComplete(_ ⇒ system.terminate()) // and shutdown when done
    case Failure(ex) =>
      println(s"Binding interface on $host:$port failed: ", ex.getMessage)
  }

}

package eu.interiot.ipsm.workshop.util

import akka.NotUsed
import akka.stream.scaladsl.Source
import akka.http.scaladsl.server.Directives._
import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.marshallers.xml.ScalaXmlSupport
import akka.stream.ActorMaterializer
import akka.http.scaladsl.marshalling.{Marshaller, ToResponseMarshaller}
import akka.http.scaladsl.model.HttpEntity.ChunkStreamPart
import akka.http.scaladsl.model._

import scala.io.StdIn
import scala.util.{Failure, Random, Success}
import com.typesafe.config.ConfigFactory
import eu.interiot.ipsm.workshop.CorsSupport

import scala.xml.NodeSeq

object HttpXmlSource extends App with ScalaXmlSupport with CorsSupport {

  implicit val system = ActorSystem()
  implicit val materializer = ActorMaterializer()
  implicit val executionContext = system.dispatcher

  def marshalPersonXml(person: Person): NodeSeq =
    <Person>
      <FirstName>
        {person.firstName}
      </FirstName>
      <LastName>
        {person.lastName}
      </LastName>
      <Id>
        {person.id}
      </Id>
    </Person>

  def marshalDeviceXml(device: XMLDevice): NodeSeq =
    <Device>
      <GPSposition>{device.position}</GPSposition>
      {if (device.name.isDefined) <name>{device.name.get}</name>}
      {if (device.description.isDefined) <description>{device.description.get}</description>}
    </Device>

  implicit val toResponseMarshaller: ToResponseMarshaller[Source[XMLDevice, NotUsed]] =
    Marshaller.opaque { items =>
      val data = items.map { item =>
        val str = marshalDeviceXml(item)
          .toString()
          .split('\n').map(_.trim)
          .mkString(" ")
        ChunkStreamPart(str + "\n")
      }
      HttpResponse(entity = HttpEntity.Chunked(ContentTypes.`text/xml(UTF-8)`, data))
    }

  def sourceOfDevices(max: Int): Source[XMLDevice, NotUsed] = {
    Source(1 to max)
      .map { n =>
        XMLDevice(entitiesHelper.randomPositionStr(), entitiesHelper.randomNameOption(), entitiesHelper.randomDescriptionOption())
      }
  }

  def sourceOfPeople(max: Int): Source[Person, NotUsed] = {
    Source(1 to max)
      .map { n =>
        Person("John", "Doe", n)
      }
  }

  val maxStreamLength: Int = 100000

  val route =
    corsHandler(
      path("xml" / IntNumber) { n =>
        complete(sourceOfDevices(n))
      } ~
        path("xml") {
          complete(sourceOfDevices(maxStreamLength))
        }
    )

  val config = ConfigFactory.load()
  val host = config.getString("xml-http.host")
  val port = config.getInt("xml-http.port")

  Http().bindAndHandle(handler = route, interface = host, port = port) onComplete {
    case Success(binding) =>
      println(s"XML stream available from: http:/${binding.localAddress}/xml[/<stream_length>]")
      println("Press <Enter> to STOP")
      StdIn.readLine() // let it run until user presses return
      binding.unbind() // trigger unbinding from the port
        .onComplete(_ ⇒ system.terminate()) // and shutdown when done
    case Failure(ex) =>
      println(s"Binding interface on $host:$port failed: ", ex.getMessage)
  }



}

package eu.interiot.ipsm.workshop.util

import akka.actor.ActorSystem
import akka.kafka.ProducerSettings
import akka.kafka.scaladsl.Producer
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Source
import com.typesafe.config.ConfigFactory
import eu.interiot.ipsm.workshop.HostSpec
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.common.serialization.{ByteArraySerializer, StringSerializer}
import org.rogach.scallop.{ScallopConf, ScallopOption}

object KafkaXmlProducer extends App {

  val config = ConfigFactory.load()
  implicit val system = ActorSystem.create("xml-producer", config)
  implicit val mat = ActorMaterializer()

  class Conf(arguments: Seq[String]) extends ScallopConf(arguments) {
    val bootstrap: ScallopOption[HostSpec] = opt[HostSpec]()(HostSpec.hostnameConverter)
    verify()
  }

  val params = new Conf(args)
  val bootstrap: String = {
    val bs = params.bootstrap.getOrElse(HostSpec.default)
    s"${bs.hostname}:${bs.port}"
  }

  val producerSettings = ProducerSettings(system, new ByteArraySerializer, new StringSerializer)
    .withBootstrapServers(bootstrap)

  val done = Source(1 to 10000)
    .map { n =>
      s"""
         |<Author>
         |<FirstName>John</FirstName>
         |<LastName>Smith</LastName>
         |<Number>$n</Number>
         |</Author>
      """.stripMargin.trim.split('\n').mkString(" ")
    }
    .map { elem =>
      Thread.sleep(200)
      new ProducerRecord[Array[Byte], String]("iot-xml2", elem)
    }
    .runWith(Producer.plainSink(producerSettings))

}

package eu.interiot.ipsm.workshop.misc

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Flow, Keep, RunnableGraph, Sink, Source}
import akka.{Done, NotUsed}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.Random

object StreamEx extends App {

  implicit val system = ActorSystem()
  implicit val materializer = ActorMaterializer()

  val source: Source[Int, NotUsed] = Source(1 to 11)

  val flow: Flow[Int, Int, NotUsed] =
    Flow[Int]
      .filter(n => n % 2 == 0)

  val sink: Sink[Int, Future[Done]] =
    Sink
      .foreach(println)

  val runnable: RunnableGraph[Future[Done]] =
    source
      .via(flow)
      .toMat(sink)(Keep.right)

  val runnable2: RunnableGraph[Future[Done]] =
    source
      .via(flow)
      .mapAsync(parallelism = 3)(elem =>
        Future({
          Thread.sleep(Random.nextInt(1000))
          elem * 2
        }).mapTo[Int])
      .toMat(sink)(Keep.right)

  runnable2.run().andThen {
    case _ => system.terminate()
  }

}

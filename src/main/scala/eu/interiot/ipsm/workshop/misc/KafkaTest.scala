package eu.interiot.ipsm.workshop.misc

import akka.actor.ActorSystem
import akka.kafka.scaladsl.Consumer
import akka.kafka.{ConsumerSettings, Subscriptions}
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Sink
import com.typesafe.config.ConfigFactory
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.common.serialization.{ByteArrayDeserializer, StringDeserializer}

import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

object KafkaTest {

  def main(args: Array[String]): Unit = {

    def uuid = java.util.UUID.randomUUID.toString

    val config = ConfigFactory.load()
    implicit val system = ActorSystem.create("kafka-consumer-demo", config)
    implicit val mat = ActorMaterializer()

    val parallelism = 3

    val consumerSettings = ConsumerSettings(system, new ByteArrayDeserializer, new StringDeserializer)
      .withBootstrapServers("localhost:9092")
      .withGroupId(uuid) // każdy klient strumieniowy powinien wykorzystywać inny identyfikator grupy
      //    .withProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest")
      .withProperty("dual.commit.enabled", "false")
      //  .withProperty("offsets.storage", "kafka")
      .withProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest")
    //      .withProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "latest")

    import system.dispatcher

    Consumer.committableSource(consumerSettings, Subscriptions.topics(args(0)))
      .mapAsync(parallelism)(msg => {
        Future {
          val rec = msg.record
          println(s"topic: ${rec.topic()}, partition: ${rec.partition()}, value: ${rec.value()}, offset: ${rec.offset()}")
          msg
        }
      })
      .runWith(Sink.ignore)

    // prevent WakeupException on quick restarts of vm
    import java.time.Instant

    import scala.language.postfixOps

    scala.sys.addShutdownHook {
      println("Terminating... - " + Instant.now)
      system.terminate()
      Await.result(system.whenTerminated, 30 seconds)
      println("Terminated... Bye - " + Instant.now)
    }
  }

}

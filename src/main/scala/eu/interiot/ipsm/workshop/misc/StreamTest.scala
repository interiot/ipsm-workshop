package eu.interiot.ipsm.workshop.misc

import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream._
import akka.stream.scaladsl._

object StreamTest extends App {
  val system = ActorSystem("LifecycleDemo")
  implicit val materializer = ActorMaterializer.create(system)
  //  import scala.concurrent.ExecutionContext.Implicits.global
  import system.dispatcher

  def processingStage(name: String): Flow[String, String, NotUsed] =
    Flow[String].map { s ⇒
      println(name + " started processing " + s + " on thread " + Thread.currentThread().getName)
      Thread.sleep(100) // Simulate long processing *don't sleep in your real code!*
      println(name + " finished processing " + s)
      s
    }

  val completion = Source(List("Hello", "Streams", "World!"))
    .via(processingStage("A")).async
    .via(processingStage("B")).async
    .via(processingStage("C")).async
    .runWith(Sink.foreach(s ⇒ println("Got output " + s)))

  completion.onComplete(_ => system.terminate())
}

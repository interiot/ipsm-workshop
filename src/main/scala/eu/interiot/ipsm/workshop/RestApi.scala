package eu.interiot.ipsm.workshop

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.model.HttpHeader.ParsingResult.Ok
import akka.http.scaladsl.model.HttpMethods._
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server._
import akka.util.Timeout

import scala.concurrent.ExecutionContext

trait RestApi extends Resources with JsonSupport with CorsSupport {
  import scala.concurrent.duration._
  implicit val executionContext: ExecutionContext
  implicit val timeout = Timeout(10.seconds)

  val idRE = """\S+""".r

  val api: Route =
  corsHandler( {
    path("channels") {
      get {
        listChannels()
      } ~
      post {
        entity(as[ChannelConfig]) { conf =>
          createChannel(conf)
        }
      }
    } ~
    path("channels" / idRE) { id =>
      delete {
        deleteChannel(id)
      }
    } ~
    path("alignments") {
      get {
        listAlignments()
      }
    } ~
      pathPrefix("swagger") {
        pathEnd {
          redirect("/swagger/", StatusCodes.TemporaryRedirect)
        } ~
          pathSingleSlash {
            getFromResource("swagger/index.html")
          } ~
          getFromResourceDirectory("swagger")
      } ~
        path("terminate") {
          for (reaper <- system.actorSelection("/user/reaper").resolveOne()) {
            reaper ! Reaper.AllDone
          }
          complete(StatusCodes.OK, SuccessResponse(s"Baby IPSM shutdown initiated"))
        }
    })
}

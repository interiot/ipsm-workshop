#!/bin/bash
if hash curl 2>/dev/null; then
    curl http://localhost:8080/terminate
else
    echo "\nERROR: curl not available!\n"
fi

#!/bin/bash
USAGE(){
    echo ""
    echo "    Usage: ./`basename $0` kafka_topic file.rdf"
    echo ""
}
if [ "$#" -ne "2" ]; then
    USAGE
    exit 1
fi
if [ ! -f "$2" ]; then
    echo ""
    echo "    RDF file \"$r2\" does not exist!"
    echo ""
    exit 1
fi
kafkacat -P -b localhost -t "$1" ../data/$2

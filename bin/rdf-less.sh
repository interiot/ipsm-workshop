#!/bin/bash
USAGE(){
    echo ""
    echo "    Usage: ./`basename $0` kafka_topic"
    echo ""
}
if [ "$#" -ne "1" ]; then
    USAGE
    exit 1
fi
kafkacat -C -b localhost -t "$1"

name := "ipsm-workshop"

version := "0.1.4"

scalaVersion := "2.12.1"

scalacOptions := Seq(
  "-unchecked",
  "-deprecation",
  "-feature",
  "-encoding",
  "utf8"
)

val banana = (name: String) => "org.w3" %% name % "0.8.4-SNAPSHOT" excludeAll (ExclusionRule(organization = "org.scala-stm"))

//add the bblfish-snapshots repository to the resolvers
resolvers += "bblfish-snapshots" at "http://bblfish.net/work/repo/snapshots"

//choose the packages you need for your dependencies
val bananaDeps = Seq("banana", "banana-rdf", "banana-jena").map(banana)

libraryDependencies ++= {
  val akkaV = "2.4.17"
  val kafkaV = "0.13"
  val akkaHttpV = "10.0.4"

  Seq(
    "org.rogach" %% "scallop" % "2.1.1",
    "ch.qos.logback" % "logback-classic" % "1.2.1",
    "com.typesafe.scala-logging" %% "scala-logging" % "3.5.0",
    "com.typesafe.akka" %% "akka-actor" % akkaV,
    "com.typesafe.akka" %% "akka-stream" % akkaV,
    "com.typesafe.akka" %% "akka-stream-kafka" % kafkaV,
    "com.typesafe.akka" %% "akka-http" % akkaHttpV,
    "com.typesafe.akka" %% "akka-http-xml" % akkaHttpV,
    "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpV
  ) ++ bananaDeps
}

javaOptions in reStart += "-Dfile.encoding=utf8"

assemblyMergeStrategy in assembly := {
  case PathList("org", "apache", "commons", "logging", xs@_*) => MergeStrategy.first
  case PathList("javax", "xml", xs@_*) => MergeStrategy.first
  case x =>
    val oldStrategy = (assemblyMergeStrategy in assembly).value
    oldStrategy(x)
}
